<?php

namespace AppBundle\DataFixtures\ORM;
use AppBundle\Entity\Todo;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Created by PhpStorm.
 * User: youssef
 * Date: 4/19/16
 * Time: 1:23 AM
 */
class LoadTodosData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $todos = [
            "Learn symfony",
            "Learn AngularJs",
            "Design database",
            "Design UI",
            "Build REST API"
        ];

        foreach ($todos as $text) {
            $todo = new Todo();
            $todo->setText($text);
            $todo->setDone(false);
            $manager->persist($todo);
        }

        $manager->flush();
    }
}