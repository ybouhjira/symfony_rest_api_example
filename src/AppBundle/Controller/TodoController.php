<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Todo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;

class TodoController extends Controller
{
    /**
     * @Route("/todos")
     * @View()
     */
    public function getAllAction()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Todo')->findAll();
    }

    /**
     * @Route("/todos")
     * @Method({"POST"})
     *
     */
    public function createAction()
    {

    }

    /**
     * @Route("/todos/{id}")
     * @ParamConverter("todo", class="AppBundle:Todo")
     * @param Todo $todo a todo found by id
     * @return Todo
     */
    public function getAction(Todo $todo)
    {
        return $todo;
    }

}
